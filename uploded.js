import * as data from "./data.js";

function getParameterByName(searhkey) {
  if (!searhkey) searhkey = document.getElementById('input-search1').value;

  var regex = RegExp(/^$|^[\u0621-\u064A\u0660-\u0669a-zA-Z0-9\-_# !,:؟?.ـ@+\r\n]+$/);
  let results = regex.exec(searhkey);
  
  if (!results) return null;

  return decodeURIComponent(results[0]);
}

function normalizeArabic(text) {
  return text.replace(/[\u0622\u0623\u0625]/g, 'ا')  // Replace all variations of 'ا'
             .replace(/[\u0649\u064A]/g, 'ي')      // Replace all variations of 'ي'
             .replace(/[\u0629]/g, 'ه');          // Replace 'ة' with 'ه'
}

function normalizeText(text) {
  return normalizeArabic(text.toLowerCase());
}

var searchIndex = data.sitesData();

(function (window, document, undefined) {

  'use strict';

  let form = document.querySelector('#form-search');
  let resultList = document.querySelector('#search-results');
  let mainpage = document.querySelector('.main-section');
  let bannersection = document.querySelector('.bannersection');
  let resultpage = document.querySelector('.search-section');

  let createHTML = function (article, id, query) {
    let html =
      '<div class="search-box" id="search-result-' + id + '">' +
      '<a target="_blank" onclick="passinput(\'' + query + '\')" rel="noopener noreferrer" href="' + article.url + '">' +
      '<h2>' + article.title + '</h2>' +
      article.content.slice(0, 150) + '...' +
      '</a>' +
      '</div><hr/>';
    return html;
  };

  let createNoResultsHTML = function () {
    var lan = $('html')[0].lang;
    if (lan == "ar") {
      return '<p>لم يتم العثور على أي نتيجة لبحثك.</p>';
    } else {
      return '<p>No matches were found.</p>';
    }
  };

  let searchErorr = function () {
    var lan = $('html')[0].lang;
    if (lan == "ar") {
      return '<p>مفتاح البحث يجب أن لا يقل عن 3 أحرف</p>';
    } else {
      return '<p>Key search must not be less than 3 characters.</p>';
    }
  };

  let createResultsHTML = function (results, query) {
    var lan = $('html')[0].lang;

    let html = lan == "ar" 
      ? '<p>تم العثور على ' + results.length + ' نتائج مطابقة</p>' 
      : '<p>Found ' + results.length + ' matching articles</p>';

    html += results.map(function (article, index) {
      return createHTML(article, index, query);
    }).join('');
    return html;
  };

  let search = function (query) {
    let normalizedQuery = normalizeText(query);
    let reg = new RegExp(normalizedQuery, 'gi');
    let priority1 = [];
    let priority2 = [];
    let priority3 = [];

    searchIndex.forEach(function (article) {
      if (reg.test(normalizeText(article.title))) return priority1.push(article);
      if (reg.test(normalizeText(article.content))) priority2.push(article);
      if (reg.test(normalizeText(article.summary))) priority3.push(article);
    });

    let results = [].concat(priority1, priority2, priority3);

    resultList.innerHTML = results.length < 1 ? createNoResultsHTML() : createResultsHTML(results, query);
  };

  let submitHandler = function (event, inputId) {
    event.preventDefault();
    var searhkey = document.getElementById(inputId).value;
    var input = getParameterByName(searhkey);
    if (input) {
      if (input.length > 2) {
        search(input);
        if (mainpage) mainpage.classList.add('d-none');
        if (bannersection) bannersection.classList.add('d-none');
        if (resultpage) resultpage.classList.add('d-block');
      } else {
        resultList.innerHTML = searchErorr();
        if (mainpage) mainpage.classList.add('d-none');
        if (bannersection) bannersection.classList.add('d-none');
        if (resultpage) resultpage.classList.add('d-block');
      }
    }
  };

  if (!form || !resultList || !searchIndex) return;

  var button = document.getElementById("submit-search1");
  var button2 = document.getElementById("submit-search");

  button.addEventListener('click', (event) => { submitHandler(event, "input-search1") });
  button2.addEventListener('click', (event) => { submitHandler(event, "input-search") });

})(window, document);

